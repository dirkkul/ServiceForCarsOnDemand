from datetime import datetime, timedelta
import json
import os
import unittest
import re
import time

from CarsOnDemand import app, db, car_search

class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        self.app.testing = True
        db.drop_all()
        db.create_all()
    
        self.assertEqual(app.debug, False)
        self.assertEqual(app.testing, True)

    # executed after each test
    def tearDown(self):
        pass

#### helper Functions ####

    def UserAdd(self, email, name, age, gender):
        data = {
                    'email': email,
                    'name': name,
                    'age': age,
                    'gender': gender
                }
        
        return self.app.post('/user', data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)

    def UserGet(self, email):
        return self.app.get('/user/'+email, follow_redirects=True)

    def UserGets(self):
        return self.app.get('/user', follow_redirects=True)

    def UserDelete(self, email):
        return self.app.delete('/user/'+email, follow_redirects=True)

    def UserUpdate(self, email, emailNew, name, age, gender):
        data = {
                    'email': emailNew,
                    'name': name,
                    'age': age,
                    'gender': gender
                }
                
        return self.app.put('/user/'+email, data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)

    def DemandAdd(self, email, PickUpLoc, DropOffLoc, PickUpFrom = None, DropOfUntil = None, Seats = 1, Engine = 1, InfoSys = 'no'):
        if PickUpFrom == None:
            PickUpFrom = datetime.now() + timedelta(minutes = 60)
            PickUpFrom = PickUpFrom.strftime("%Y-%m-%d %H:%M")
        if DropOfUntil == None: 
            DropOfUntil = datetime.now() + timedelta(minutes = 120)
            DropOfUntil = DropOfUntil.strftime("%Y-%m-%d %H:%M")
        
        data = {
                    'email': email,
                    'PickUpFrom': PickUpFrom,
                    'DropOfUntil': DropOfUntil,
                    'PickUpLoc': PickUpLoc,
                    'DropOffLoc': DropOffLoc,
                    'Seats': Seats,
                    'Engine': Engine,
                    'InfoSys': InfoSys
                }
        
        return self.app.post('/demand', data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)

    def DemandGet(self, id):
        return self.app.get('/demand/'+str(id), follow_redirects=True)

    def DemandsGet(self):
        return self.app.get('/demand', follow_redirects=True)

    def DemandDelete(self, id):
        return self.app.delete('/demand/'+str(id), follow_redirects=True)

    def DemandDeleteAll(self):
        return self.app.delete('/demand/DelAll', follow_redirects=True)

    def DemandUpdate(self, id, PickUpFrom = None, DropOfUntil = None, PickUpLoc = None, DropOffLoc = None, Seats = None, Engine = None):
        data = {
                    'PickUpFrom': PickUpFrom,
                    'DropOfUntil': DropOfUntil,
                    'PickUpLoc': PickUpLoc,
                    'DropOffLoc': DropOffLoc,
                    'Seats': Seats,
                    'Engine': Engine
                }
                
        return self.app.put('/demand/'+str(id), data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)



    def CarAdd(self, Model, Engine, InfoSys, IntDesign, Seats, Location = 0):
        data = {
                    'Model': Model,
                    'Engine': Engine,
                    'InfoSys': InfoSys,
                    'IntDesign': IntDesign,
                    'Seats': Seats,
                    'Location': Location
                }
        
        return self.app.post('/car', data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)

    def CarGet(self, id):
        return self.app.get('/car/'+str(id), follow_redirects=True)

    def CarsGet(self):
        return self.app.get('/car', follow_redirects=True)
        
    def CarDelete(self, id):
        return self.app.delete('/car/'+str(id), follow_redirects=True)

    def CarSearch(self, UserLoc = None, Model = None, Engine = 1, InfoSys = None, Seats = 1):
        data = {
                    'UserLoc' : UserLoc,
                    'Model': Model,
                    'Engine': Engine,
                    'InfoSys': InfoSys,
                    'Seats': Seats
                }
                
        return self.app.post('/car/search', data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)


    def CarUpdate(self, id, Model = None, Engine = None, InfoSys = None, IntDesign = None, Location = None, Seats = None):
        data = {
                    'Model': Model,
                    'Engine': Engine,
                    'InfoSys': InfoSys,
                    'IntDesign': IntDesign,
                    'Location': Location,
                    'Seats': Seats
                }
                
        return self.app.put('/car/'+str(id), data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)
    
    def JsonToDatetime(self, s):
        s = s.replace('+00:00','')
        s = s.replace('T',' ')
        s = re.sub('\.[0-9]*', '', s)
        return datetime.strptime( s, "%Y-%m-%d %H:%M:%S")

###############
#### tests ####
###############
    
    def test_required_valuefunction(self):
        """ Forget a required value, age is missing."""
        data = {'email': 'user@test.de', 'name': 'a', 'gender': 'w'}
        return self.app.post('/user', data=json.dumps(data),
                              content_type='application/json',
                               follow_redirects=True)

    def test_add_user(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        self.assertEqual(response.status_code, 201)
        
        response = self.UserGet('user@test.de')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['age'], 19)
        self.assertEqual(json.loads(response.get_data(as_text=True))['email'], 'user@test.de')
        self.assertEqual(json.loads(response.get_data(as_text=True))['name'], 'Name')
        self.assertEqual(json.loads(response.get_data(as_text=True))['gender'], 'm')
        
        #try to add the user again
        response = self.UserAdd('user@test.de', 'Name', 25, 'w')
        self.assertEqual(response.status_code, 406)
        
        #add a user that's too young
        response = self.UserAdd('user2@test.de', 'Name', 17, 'm')
        self.assertEqual(response.status_code, 406)
        
        #now add a second user and check that there are two entries in the db
        response = self.UserAdd('user2@test.de', 'Name2', 25, 'w')
        self.assertEqual(response.status_code, 201)
        response = self.UserGets()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 2)
        
    def test_delete_user(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        self.assertEqual(response.status_code, 201)
        
        #first delete the wrong user
        response = self.UserDelete('user2@test.de')
        self.assertEqual(response.status_code, 406)
        
        #add a demand and check that we cannot delete the user while it is active
        response = self.CarAdd('Golf', 2500, 'Yes', 'red', 4)
        self.DemandAdd('user@test.de', 2, 10)
        response = self.UserDelete('user@test.de')
        self.assertEqual(response.status_code, 406)
        self.DemandDeleteAll()
        
        #now the correct one after deleting the demand
        response = self.UserDelete('user@test.de')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['email'], 'user@test.de')
        
        #db should be empty now
        response = self.UserGets()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True)), [])

    def test_update_user(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        self.assertEqual(response.status_code, 201)
        
        #update the wrong user
        response = self.UserUpdate('user2@test.de', 'Name2', 'user3@test.de', 20, 'w')
        self.assertEqual(response.status_code, 406)
        
        #add a second user and try to change the email adress of the first user to this one
        response = self.UserAdd('user3@test.de', 'Name3', 19, 'm')
        self.assertEqual(response.status_code, 201)
        response = self.UserUpdate('user@test.de','user3@test.de', 'Name3', 20, 'w')
        self.assertEqual(response.status_code, 406)
        
        #now change the all fields of the first user and verify that the changes happened
        response = self.UserUpdate('user@test.de','user2@test.de', 'Name2', 20, 'w')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['email'], 'user2@test.de')
        self.assertEqual(json.loads(response.get_data(as_text=True))['name'], 'Name2')
        self.assertEqual(json.loads(response.get_data(as_text=True))['age'], 20)
        self.assertEqual(json.loads(response.get_data(as_text=True))['gender'], 'w')
        
        #check that new email is in db and old email is gone
        response = self.UserGet('user2@test.de')
        self.assertEqual(response.status_code, 200)
        response = self.UserGet('user@test.de')
        self.assertEqual(response.status_code, 406)
    
    def test_add_car(self):
        response = self.CarAdd('Golf', 2000, 'Yes', 'red', 4, Location = 5)
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        
        response = self.CarGet(id)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.get_data(as_text=True))
        self.assertEqual(response_json['Model'], 'Golf')
        self.assertEqual(response_json['Engine'], 2000)
        self.assertEqual(response_json['InfoSys'], 1)
        self.assertEqual(response_json['Seats'], 4)
        self.assertEqual(response_json['Location'], 5)
        self.assertEqual(response_json['Total'], 0)
        
        #add a car in the wrong color:
        response = self.CarAdd('Golf', 2000, 'Yes', 'purple', 4)
        self.assertEqual(response.status_code, 406)
        
        #add a second car and ensure there are two cars now
        response = self.CarAdd('Golf', 3000, 'Yes', 'red', 2)
        self.assertEqual(response.status_code, 200)
        response = self.CarsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 2)

    def test_delete_car(self):
        response = self.CarAdd('Golf', 1500, 'Yes', 'red', 2)
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        
        #delete the wrong car
        response = self.CarDelete(id+1)
        self.assertEqual(response.status_code, 406)
        
        #add a demand and check that we cannot delete the car while it is active
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        self.DemandAdd('user@test.de', 2, 10)
        response = self.CarDelete(id)
        self.assertEqual(response.status_code, 406)
        self.DemandDeleteAll()
        
        #no delete the right one and check that car is really gone
        response = self.CarDelete(id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['id'], id)
        
        response = self.CarsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 0)

    def test_update_car(self):
        response = self.CarAdd('Golf', 2500, 'Yes', 'red', 3)
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        
        #first, test updating the wrong car
        response = self.CarUpdate(id+1,'Golf 5', 2500, 'Yes')
        self.assertEqual(response.status_code, 406)
        
        #only update the location and ensure that the other fields did not change
        response = self.CarUpdate(id, Location = -5)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['id'], id)
        self.assertEqual(json.loads(response.get_data(as_text=True))['Model'], 'Golf')
        self.assertEqual(json.loads(response.get_data(as_text=True))['Engine'], 2500)
        self.assertEqual(json.loads(response.get_data(as_text=True))['InfoSys'], 1)
        self.assertEqual(json.loads(response.get_data(as_text=True))['IntDesign'], 'red')
        self.assertEqual(json.loads(response.get_data(as_text=True))['Seats'], 3)
        self.assertEqual(json.loads(response.get_data(as_text=True))['Location'], -5)
        
        #now update everything
        response = self.CarUpdate(id, Model ='Golf 5', Engine = 3500, InfoSys = 'No', IntDesign = 'black', Location = 5, Seats = 6)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['id'], id)
        self.assertEqual(json.loads(response.get_data(as_text=True))['Model'], 'Golf 5')
        self.assertEqual(json.loads(response.get_data(as_text=True))['Engine'], 3500)
        self.assertEqual(json.loads(response.get_data(as_text=True))['InfoSys'], 0)
        self.assertEqual(json.loads(response.get_data(as_text=True))['IntDesign'], 'black')
        self.assertEqual(json.loads(response.get_data(as_text=True))['Location'], 5)
        self.assertEqual(json.loads(response.get_data(as_text=True))['Seats'], 6)
        
        #ensure that there is still only one car in the db
        response = self.CarsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 1)

    def test_search_car(self):
        response = self.CarAdd('Golf', 2500, 'Yes', 'red', 3, Location = 1)
        self.CarAdd('Astra', 2500, 'Yes', 'red', 5, Location = -3)
        self.CarAdd('Passat', 2500, 'No', 'red', 6, Location = 6)
        
        #Check that there are 3 cars in the db
        response = self.CarsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 3)
        
        #We search for a car with 5 seats wit the closest location to 5.
        response = self.CarSearch(Seats=5, UserLoc= 5)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))[0]['id'], 3)
        
        #No added car has 10 seats
        response = self.CarSearch(Seats=10, UserLoc= 5)
        self.assertEqual(response.status_code, 406)
        
    def test_add_demand(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        response = self.CarAdd('Golf', 2500, 'Yes', 'red', 2)
        
        #add demand that has to many seats
        response = self.DemandAdd('user@test.de', 2, 10, Seats = 4)
        self.assertEqual(response.status_code, 406)
        
        #wrong time
        StartTimedt = datetime.now() + timedelta(minutes = 60 )
        EndTimedt = datetime.now() + timedelta(minutes = 70 )
        StartTime = StartTimedt.strftime("%Y-%m-%d %H:%M")
        EndTime = EndTimedt.strftime("%Y-%m-%d %H:%M")
        response = self.DemandAdd('user@test.de', -5, 10, PickUpFrom = EndTime, DropOfUntil = StartTime)
        self.assertEqual(response.status_code, 406)
        
        #time too short
        response = self.DemandAdd('user@test.de', -5, 1000, PickUpFrom = StartTime, DropOfUntil = EndTime)
        self.assertEqual(response.status_code, 406)
        
        #add demand and check that every value was added
        response = self.DemandAdd('user@test.de', 2, 10, Seats = 2, PickUpFrom = StartTime, DropOfUntil = EndTime, Engine=2000, InfoSys = 'yes')
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        
        response = self.DemandGet(id)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.get_data(as_text=True))
        self.assertEqual(response_json['email'], 'user@test.de')
        self.assertEqual(response_json['PickUpFrom']  , StartTimedt.strftime("%Y-%m-%dT%H:%M")+':00+00:00' )
        self.assertEqual(response_json['DropOfUntil']  , EndTimedt.strftime("%Y-%m-%dT%H:%M")+':00+00:00' )
        self.assertEqual(float(response_json['PickUpLoc']), 2)
        self.assertEqual(float(response_json['DropOffLoc']), 10)
        self.assertEqual(int(response_json['Seats']), 2)
        self.assertEqual(int(response_json['Engine']), 2000)
        self.assertEqual(int(response_json['InfoSys']), 1)
        
        #Check assigned demand times. Rental has to start before PickupTime (to drove to user location)
        #and end before the last possible drop off
        DemandStart1 = self.JsonToDatetime(response_json['DemandStart'])
        DemandEnd1 = self.JsonToDatetime(response_json['DemandEnd'])
        
        self.assertTrue(DemandStart1 < StartTimedt)
        self.assertTrue(DemandEnd1 < EndTimedt)
        
        #We now add an earlier demand to test if the Starttime gehts shifted accordingly
        StartTime2 = (datetime.now() + timedelta(minutes = 10 )).strftime("%Y-%m-%d %H:%M")
        EndTime2 = (datetime.now() + timedelta(minutes = 20 )).strftime("%Y-%m-%d %H:%M")
        response = self.DemandAdd('user@test.de', 0, -2, PickUpFrom = StartTime2, DropOfUntil = EndTime2)
        self.assertEqual(response.status_code, 200)
        
        #The car is now two units further away, therefore the demand start time needs to be shifted 2min forwards
        response = self.DemandGet(1)
        response_json = json.loads(response.get_data(as_text=True))
        DemandStart2 = self.JsonToDatetime(response_json['DemandStart'])
        self.assertEqual(DemandStart2 + timedelta(minutes=2), DemandStart1)
        
        #Add one more demand, to check if the start time gets also modified when we add a demand between two existing demands
        StartTime2 = (datetime.now() + timedelta(minutes = 20 )).strftime("%Y-%m-%d %H:%M")
        EndTime2 = (datetime.now() + timedelta(minutes = 40 )).strftime("%Y-%m-%d %H:%M")
        response = self.DemandAdd('user@test.de', -3, -4, PickUpFrom = StartTime2, DropOfUntil = EndTime2)
        self.assertEqual(response.status_code, 200)
        
        response = self.DemandGet(1)
        response_json = json.loads(response.get_data(as_text=True))
        DemandStart3 = self.JsonToDatetime(response_json['DemandStart'])
        self.assertEqual(DemandStart3 + timedelta(minutes=4), DemandStart1)

        # ~ #Add more demands to check and check that the number of demands fits
        response = self.DemandAdd('user@test.de', 2, 10)
        self.assertEqual(response.status_code, 200)
        response = self.DemandAdd('user@test.de', 2, 10)
        self.assertEqual(response.status_code, 200)
        
        response = self.DemandsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 5)

    def test_add_demand_twoCars(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        response = self.CarAdd('Golf', 2000, 'Yes', 'red', 2)
        response = self.CarAdd('Mercedes', 2500, 'Yes', 'red', 4)
        id = json.loads(response.get_data(as_text=True))['id']
        response = self.CarUpdate(id, Location = -1)
        
        StartTimedt = datetime.now() + timedelta(minutes = 60 )
        EndTimedt = datetime.now() + timedelta(minutes = 100 )
        StartTime = StartTimedt.strftime("%Y-%m-%d %H:%M")
        EndTime = EndTimedt.strftime("%Y-%m-%d %H:%M")
        
        #We now add demands and check that the closest car is assigned. We want to minimize the time cars drive empty
        #Car 1 is closer (1: 0, 2: -1)
        response = self.DemandAdd('user@test.de', 2, 10, PickUpFrom = StartTime, DropOfUntil = EndTime)
        id = json.loads(response.get_data(as_text=True))['AssignedCar']
        Car1Dem1Start = json.loads(response.get_data(as_text=True))['DemandStart']
        Car1Dem1End = json.loads(response.get_data(as_text=True))['DemandEnd']
        self.assertEqual(id, 1)
        
        #Car 2 is closer (1: 10, 2: -1)
        response = self.DemandAdd('user@test.de', 2, 9, PickUpFrom = StartTime, DropOfUntil = EndTime)
        id = json.loads(response.get_data(as_text=True))['AssignedCar']
        Car2Dem1Start = json.loads(response.get_data(as_text=True))['DemandStart']
        Car2Dem1End = json.loads(response.get_data(as_text=True))['DemandEnd']
        self.assertEqual(id, 2)
        
        #Car 2 is closer (1: 10, 2: 9)
        response = self.DemandAdd('user@test.de', 7, -5, PickUpFrom = StartTime, DropOfUntil = EndTime)
        id = json.loads(response.get_data(as_text=True))['AssignedCar']
        Car2Dem2Start = json.loads(response.get_data(as_text=True))['DemandStart']
        Car2Dem2End = json.loads(response.get_data(as_text=True))['DemandEnd']
        self.assertEqual(id, 2)
        
        #Car 1 is closer (1: 10, 2: -5)
        response = self.DemandAdd('user@test.de', 7, -4, PickUpFrom = StartTime, DropOfUntil = EndTime)
        id = json.loads(response.get_data(as_text=True))['AssignedCar']
        Car1Dem2Start = json.loads(response.get_data(as_text=True))['DemandStart']
        Car1Dem2End = json.loads(response.get_data(as_text=True))['DemandEnd']
        self.assertEqual(id, 1)
        
        #Car 1 is closer (1: -4, 2: -5)
        response = self.DemandAdd('user@test.de', -4, 1, PickUpFrom = StartTime, DropOfUntil = EndTime)
        id = json.loads(response.get_data(as_text=True))['AssignedCar']
        Car1Dem3Start = json.loads(response.get_data(as_text=True))['DemandStart']
        Car1Dem3End = json.loads(response.get_data(as_text=True))['DemandEnd']
        self.assertEqual(id, 1)
        
        #Car 2 is closer (1: 1, 2: -5)
        response = self.DemandAdd('user@test.de', -4, 1, PickUpFrom = StartTime, DropOfUntil = EndTime)
        id = json.loads(response.get_data(as_text=True))['AssignedCar']
        Car2Dem3Start = json.loads(response.get_data(as_text=True))['DemandStart']
        Car2Dem3End = json.loads(response.get_data(as_text=True))['DemandEnd']
        self.assertEqual(id, 2)
        
        #Both cars are full for the requested time and trip
        response = self.DemandAdd('user@test.de', -4, 1, PickUpFrom = StartTime, DropOfUntil = EndTime)
        self.assertEqual(response.status_code, 406)
        
        #Now Check that all the demand times are in the right order and dont overlap
        Car1Dem1Start = self.JsonToDatetime(Car1Dem1Start)
        Car1Dem1End = self.JsonToDatetime(Car1Dem1End)
        Car1Dem2Start = self.JsonToDatetime(Car1Dem2Start)
        Car1Dem2End = self.JsonToDatetime(Car1Dem2End)
        Car1Dem3Start = self.JsonToDatetime(Car1Dem3Start)
        Car1Dem3End = self.JsonToDatetime(Car1Dem3End)
        
        self.assertTrue(Car1Dem1Start < Car1Dem1End)
        self.assertTrue(Car1Dem1End < Car1Dem2Start)
        self.assertTrue(Car1Dem2Start < Car1Dem2End)
        self.assertTrue(Car1Dem2End < Car1Dem3Start)
        self.assertTrue(Car1Dem3Start < Car1Dem3End)
        self.assertTrue(Car1Dem3End < EndTimedt)
        
        Car2Dem1Start = self.JsonToDatetime(Car2Dem1Start)
        Car2Dem1End = self.JsonToDatetime(Car2Dem1End)
        Car2Dem2Start = self.JsonToDatetime(Car2Dem2Start)
        Car2Dem2End = self.JsonToDatetime(Car2Dem2End)
        Car2Dem3Start = self.JsonToDatetime(Car2Dem3Start)
        Car2Dem3End = self.JsonToDatetime(Car2Dem3End)
        
        self.assertTrue(Car2Dem1Start < Car2Dem1End)
        self.assertTrue(Car2Dem1End < Car2Dem2Start)
        self.assertTrue(Car2Dem2Start < Car2Dem2End)
        self.assertTrue(Car2Dem2End < Car2Dem3Start)
        self.assertTrue(Car2Dem3Start < Car2Dem3End)
        self.assertTrue(Car2Dem3End < EndTimedt)

    def test_expiration(self):
        """ Here we check the timer function that deletes demands that have passed. We choose a very short trip
            to ensure that it is over after the sleep time."""
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        response = self.CarAdd('Golf', 3000, 'Yes', 'red', 9)
        StartTime = (datetime.now() + timedelta(minutes = 0 ) ).strftime("%Y-%m-%d %H:%M")
        EndTime = (datetime.now() + timedelta(minutes = 3 ) ).strftime("%Y-%m-%d %H:%M")
        response = self.DemandAdd('user@test.de', -0.01, 0.02, PickUpFrom = StartTime, DropOfUntil = EndTime )
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        DemandStart = json.loads(response.get_data(as_text=True))['DemandStart']
        DemandEnd = json.loads(response.get_data(as_text=True))['DemandEnd']
        
        DemandStart = self.JsonToDatetime(DemandStart)
        DemandEnd = self.JsonToDatetime(DemandEnd)
        
        time.sleep(2)
        
        #Demand should be expired now
        response = self.DemandsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 0)
        
        #check if cars location and total distance was updated
        response = self.CarGet(1)
        self.assertEqual(json.loads(response.get_data(as_text=True))['Location'], 0.02)
        self.assertEqual(json.loads(response.get_data(as_text=True))['Total'], 0.04)

    def test_delete_demand(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        response = self.CarAdd('Golf', 3000, 'Yes', 'red', 9)
        response = self.DemandAdd('user@test.de', -5, 10)
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        
        #delete a non-existing demand
        response = self.DemandDelete(id+1)
        self.assertEqual(response.status_code, 406)
        
        #delete the correct one and ensure that the db is empty
        response = self.DemandDelete(id)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.get_data(as_text=True))['id'], id)
        
        response = self.DemandGet(id)
        self.assertEqual(response.status_code, 406)
        
    def test_delete_all_demands(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        response = self.CarAdd('Golf', 3000, 'Yes', 'red', 9)
        response = self.DemandAdd('user@test.de', 2, 3)
        response = self.DemandAdd('user@test.de', 3, 2)
        response = self.DemandAdd('user@test.de', 2, 3)
        response = self.DemandAdd('user@test.de', 3, 2)
        response = self.DemandsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 4)
        
        self.DemandDeleteAll()
        response = self.DemandsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 0)

    def test_update_demand(self):
        response = self.UserAdd('user@test.de', 'Name', 19, 'm')
        response = self.CarAdd('Golf', 2500, 'Yes', 'red', 9)
        response = self.DemandAdd('user@test.de', -5, 10)
        self.assertEqual(response.status_code, 200)
        id = json.loads(response.get_data(as_text=True))['id']
        
        #update
        StartTime2dt = (datetime.now() + timedelta(minutes = 160 ) )
        StartTime2 = StartTime2dt.strftime("%Y-%m-%d %H:%M")
        EndTime2 = (datetime.now() + timedelta(minutes = 175 ) ).strftime("%Y-%m-%d %H:%M")
        response = self.DemandUpdate(id, PickUpFrom =StartTime2, DropOfUntil = EndTime2, PickUpLoc = -4, DropOffLoc = 9, Seats = 3, Engine = 2000)
        self.assertEqual(response.status_code, 200)
        
        #check that everything got updated (time testing is tricky so we only check it roughly)
        response_json = json.loads(response.get_data(as_text=True))
        self.assertEqual(response_json['id'], id)
        self.assertEqual(float(response_json['PickUpLoc']), -4)
        self.assertEqual(float(response_json['DropOffLoc']), 9)
        self.assertEqual(int(response_json['Seats']), 3)
        self.assertEqual(int(response_json['Engine']), 2000)
        
        DemandStart = self.JsonToDatetime(response_json['DemandStart'])
        DemandEnd = self.JsonToDatetime(response_json['DemandEnd'])
        self.assertTrue(StartTime2dt- timedelta(minutes = 3 ) > DemandStart)
        self.assertTrue(StartTime2dt- timedelta(minutes = 5 ) < DemandStart)
        self.assertEqual(DemandStart + timedelta(minutes = 17 ), DemandEnd) #17min is total driving time
        
        #update only partialy
        StartTimedt = datetime.now() + timedelta(minutes = 60 )
        StartTime = StartTimedt.strftime("%Y-%m-%d %H:%M")
        response = self.DemandUpdate(id, PickUpFrom =StartTime, DropOffLoc = 8, Engine = 2100)
        self.assertEqual(response.status_code, 200)
        response_json = json.loads(response.get_data(as_text=True))
        self.assertEqual(float(response_json['PickUpLoc']), -4)
        self.assertEqual(float(response_json['DropOffLoc']), 8)
        self.assertEqual(int(response_json['Seats']), 3)
        self.assertEqual(int(response_json['Engine']), 2100)
        self.assertEqual(response_json['id'], id)

        DemandStart = self.JsonToDatetime(response_json['DemandStart'])
        DemandEnd = self.JsonToDatetime(response_json['DemandEnd'])
        self.assertTrue(StartTimedt - timedelta(minutes = 3 ) > DemandStart)
        self.assertTrue(StartTimedt - timedelta(minutes = 5 ) < DemandStart)
        self.assertEqual(DemandStart + timedelta(minutes = 16 ), DemandEnd)
        
        #There should be only one demand
        response = self.DemandsGet()
        self.assertEqual(len(json.loads(response.get_data(as_text=True))), 1)
        
        #Now add a second demand and create a situation where we cannot fulfill the update. 
        #Nothing (including demand id) should change)
        response = self.DemandAdd('user@test.de', 8, 10)
        self.assertEqual(response.status_code, 200)
        id2 = json.loads(response.get_data(as_text=True))['id']
        
        StartTime = DemandStart.strftime("%Y-%m-%d %H:%M")
        EndTime = DemandEnd.strftime("%Y-%m-%d %H:%M")
        response = self.DemandUpdate(id2, PickUpFrom = StartTime, DropOfUntil = EndTime)
        self.assertEqual(response.status_code, 406)
        response = self.DemandGet(id2)
        self.assertEqual(response.status_code, 200)

if __name__ == "__main__":
    unittest.main()
