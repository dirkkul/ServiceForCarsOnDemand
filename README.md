# Service for cars on demand
This creates a python3 web-service to manage a mobility on demand scenario using `flask` and `SQLAlchemy`. Users can register themselves and add demands for transportation. The service will assign a car or deny the request if no car is available.

The API documentation is available at https://documenter.getpostman.com/view/5339934/RWaHypTQ . Required python3 modules can be found in `requirements.txt`.

# Usage
To create a database open a python3 shell and then type `from CarsOnDemand import db` and `db.create_all()` in this shell. There might be error messages before the database is created. You can ignore them here.  
Afterwards, the service can then be started with `python3 CarsOnDemand.py` and it will be running at `http://127.0.0.1:5000/`.

After starting the service, you can add users and cars. These can be updated and deleted. A user can submit demands for transportation, that include an earliest pick up time, a latest drop off time, locations and various car features. Th closest car, that fulfills all given requirements and has a free time slot will be assigned.

Expired demands (end time is earlier than current time) will be deleted automatically, the current car location and its total driving distance will be updated.

# Testing
The `test_service.py` file includes tests for all API endpoints and can be started with `python3 test_service.py`. For each endpoint I've tried to cover edge cases and possible failes. In addition, I tried to design the tests and code to maximise the code coverage, which is now at 99%.

# Missing/To do
-   You cannot choose a car model or interior design color (the values are already in the car db), when adding a demand.
-   Dashboard for car usage, travel patterns etc
-   html files for endpoints
-   a simulator that adds random demands