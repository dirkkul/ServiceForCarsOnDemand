#From Dirk Kulawiak, check https://gitlab.com/dirkkul/ServiceForCarsOnDemand for more details

from flask import Flask, request, jsonify, make_response, abort
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
from flask_marshmallow import Marshmallow
import os, time
import numpy as np
from datetime import datetime, timedelta
import json
from apscheduler.schedulers.background import BackgroundScheduler

#based on https://medium.com/python-pandemonium/build-simple-restful-api-with-python-and-flask-part-2-724ebf04d12

app = Flask(__name__)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'CarsOnDemand.sqlite')
db = SQLAlchemy(app)
ma = Marshmallow(app)

def ExpiredDemands():
    """We check for expired demands, delete them und update the cars current location."""
    demands = Demand.query.filter( 
            (Demand.DemandEnd < datetime.now()) 
            ).order_by(Demand.DemandStart).all()
    for demand in demands:
        car = Car.query.get(demand.AssignedCar)
        car.Total += np.abs(demand.DropOffLoc - demand.PickUpLoc) + np.abs(car.Location - demand.PickUpLoc)
        car.Location = demand.DropOffLoc
        db.session.delete(demand)
    if demands != 0:
        db.session.commit()

#Add a scheduler that removes the expired demands.
scheduler = BackgroundScheduler()
app.testing = True #remove later, passing this in from an unit test doesn't work (yet)
if app.testing==True:
    scheduler.add_job(ExpiredDemands, 'interval', seconds=1)
# ~ else:
    # ~ scheduler.add_job(ExpiredDemands, 'interval', minutes=1)
scheduler.start()


### Database schemes ###

class User(db.Model):
    email = db.Column(db.String(120), primary_key=True)
    gender = db.Column(db.String(120))
    name = db.Column(db.String(120))
    age = db.Column(db.Integer())

    def __init__(self, email, name, gender, age):
        self.email = email
        self.name = name
        self.gender = gender
        self.age = age

class UserSchema(ma.Schema):
    class Meta:
        # Fields to expose
        fields = ('email', 'name', 'gender', 'age')

user_schema = UserSchema()
users_schema = UserSchema(many=True)

class Demand(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), db.ForeignKey('user.email'))
    AssignedCar = db.Column(db.Integer(), db.ForeignKey('car.id'))
    DemandStart = db.Column(db.DateTime)
    DemandEnd = db.Column(db.DateTime)
    PickUpFrom = db.Column(db.DateTime)
    DropOfUntil = db.Column(db.DateTime)
    PickUpLoc = db.Column(db.Float())
    DropOffLoc = db.Column(db.Float())
    Seats = db.Column(db.Integer())
    Engine = db.Column(db.Integer())
    InfoSys = db.Column(db.Integer())

    def __init__(self, email, AssignedCar, DemandStart, DemandEnd, PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc, Seats, Engine, InfoSys):
        self.email = email
        self.AssignedCar = AssignedCar
        self.PickUpFrom = PickUpFrom
        self.DropOfUntil = DropOfUntil
        self.DemandStart = DemandStart
        self.DemandEnd = DemandEnd
        self.PickUpLoc = PickUpLoc
        self.DropOffLoc = DropOffLoc
        self.Seats = Seats
        self.Engine = Engine
        self.InfoSys = InfoSys

class DemandSchema(ma.Schema):
    class Meta:
        fields = ('id', 'email', 'AssignedCar', 'DemandStart', 'DemandEnd', 'PickUpFrom'
            , 'DropOfUntil', 'PickUpLoc', 'DropOffLoc', 'Seats', 'Engine', 'InfoSys')

demand_schema = DemandSchema()
demands_schema = DemandSchema(many=True)

CarColors = ['red', 'green', 'blue', 'yellow', 'darkblue', 'black']
class Car(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Model = db.Column(db.String(120))
    Engine = db.Column(db.Integer())
    InfoSys = db.Column(db.Integer())
    IntDesign = db.Column(db.String(120))
    Seats = db.Column(db.Integer())
    Location = db.Column(db.Float())
    Total = db.Column(db.Float())

    def __init__(self, Model, Engine, InfoSys, IntDesign, Seats, Location, Total):
        self.Model = Model
        self.Engine = Engine
        self.IntDesign = IntDesign
        self.Seats = Seats
        self.Location = Location
        self.Total = Total
        self.InfoSys = InfoSys


class CarSchema(ma.Schema):
    class Meta:
        fields = ('id', 'Model', 'Engine', 'InfoSys', 'IntDesign', 'Location', 'Total', 'Seats')

car_schema = CarSchema()
cars_schema = CarSchema(many=True)

#helper functions
def JsonValRequired(cont, value):
    """ Helper function to read required values from the request and abort if the value is missing."""
    val = cont.get(value, None)
    if val == None:
        abort(406, value+' required \n')
    return val

#### CAR FUNCTIONS ####

def CheckCarNoQuery(car, id):
    """ Checks if a car with the given id exists. """
    if car == None:
        abort(406, 'Car {id} does not exists \n'.format(id=id))

def InfoSysToInt(InfoSys):
    if InfoSys == 'Yes' or InfoSys == 'yes':
        InfoSys = 1
    else:
        InfoSys = 0
    return InfoSys

def CheckCarColors(IntDesign):
    if IntDesign not in CarColors:
        abort(406, 'Cars can only have the following colors: '+' ,'.join(CarColors))

@app.route("/car", methods=["GET"])
def cars_get():
    all_cars = Car.query.all()
    result = cars_schema.dump(all_cars)
    return jsonify(result.data)
    
@app.route("/car/<id>", methods=["GET"])
def car_detail(id):
    car = Car.query.get(id)
    CheckCarNoQuery(car, id)  
    return car_schema.jsonify(car)

@app.route("/car/<id>", methods=["DELETE"])
def car_delete(id):
    car = Car.query.get(id)
    CheckCarNoQuery(car, id)
    
    ActiveDemand = Demand.query.filter((Demand.AssignedCar == id)).first()
    if ActiveDemand != None:
        abort(406, 'Car {id} is assigned to an active demand\n'.format(id=id))
    
    db.session.delete(car)
    db.session.commit()

    return car_schema.jsonify(car)
    
@app.route("/car/<id>", methods=["PUT"])
def car_update(id):
    """ Updates a given car entry. Every entry can be updated independently"""
    car = Car.query.get(id)
    CheckCarNoQuery(car, id)
    
    content = request.json
    Model = content.get('Model', None)
    Engine = content.get('Engine', None)
    InfoSys = content.get('InfoSys', None)
    IntDesign = content.get('IntDesign', None)
    Location = content.get('Location', None)
    Seats = content.get('Seats', None)
    if Model != None:
        car.Model = Model
    if Engine != None:
        car.Engine = int(Engine)
    if InfoSys != None:
        car.InfoSys = InfoSysToInt(InfoSys)
    if IntDesign != None:
        CheckCarColors(IntDesign)
        car.IntDesign = IntDesign
    if Location != None:
        car.Location = float(Location)
    if Seats != None:
        car.Seats = int(Seats)

    db.session.commit()
    return car_schema.jsonify(car)

@app.route("/car", methods=["POST"])
def car_add():
    content = request.json
    Model = JsonValRequired(content, 'Model')
    Engine = JsonValRequired(content, 'Engine')
    InfoSys = JsonValRequired(content, 'InfoSys')
    InfoSys = InfoSysToInt(InfoSys)
    IntDesign = JsonValRequired(content, 'IntDesign')
    CheckCarColors(IntDesign)
    Seats = JsonValRequired(content, 'Seats')
    Location = content.get('Location', 0)
    Total = 0
    
    new_car = Car(Model, int(Engine), InfoSys, IntDesign, int(Seats), float(Location), Total)

    db.session.add(new_car)
    db.session.commit()

    return car_schema.jsonify(new_car)

@app.route("/car/search", methods=["POST"])
def car_search_api():
    """Api access to search for cars. """
    content = request.json
    Seats = content.get('Seats', 1)
    Engine = content.get('Engine', 1)
    InfoSys = content.get('InfoSys', 'No')
    InfoSys = InfoSysToInt(InfoSys)
    UserLoc = JsonValRequired(content, 'UserLoc')
    
    return cars_schema.jsonify(car_search(int(Seats), int(Engine), InfoSys, float(UserLoc)))
    
def car_search(Seats, Engine, InfoSys, UserLoc):
    """ We search for cars that fulfill our requirements. """
    result = Car.query.filter(
        (Car.Seats >= Seats) &
        (Car.InfoSys >= InfoSys) & # always true if user didn't request Information System
        (Car.Engine >= Engine)
        ).order_by(func.abs(Car.Location- UserLoc)).all()
    
    if len(result) == 0:
        abort(406, 'No car fulfills requirements\n')
    
    return result   


###### DEMAND FUNCTIONS ######
def CheckDemandNoQuery(demand, id):
    """ Checks if a given demand exists."""
    if demand == None:
        abort(406, 'Demand {id} does not exists \n'.format(id=id))

def CheckDemandTime(PickUpFrom, DropOfUntil):
    """ PickUpFrom time has to be before DropOfUntil time. """
    if PickUpFrom > DropOfUntil:
        abort(406, 'PickUpFrom {pick} cannot be after DropOfUntil {drop} \n'.format(pick=PickUpFrom,drop=DropOfUntil))
    # ~ if PickUpFrom.time() < datetime.now().time():
        # ~ abort(406, 'PickUpFrom {pick} has to be in the future\n'.format(pick=PickUpFrom.time()))


@app.route("/demand/<id>", methods=["GET"])
def demand_detail(id):
    demand = Demand.query.get(id)
    CheckDemandNoQuery(demand, id)  
    return demand_schema.jsonify(demand)

@app.route("/demand", methods=["GET"])
def demands_get():
    all_demand = Demand.query.all()
    result = demands_schema.dump(all_demand)
    return jsonify(result.data)

def CompPickUpTimes(DrivingTime, PickUpFrom, DropOfUntil, DropOffLoc, PickUpLoc):
    """ Computes the time it takes to drive from current location to the pickup time,
        the total driving time, and how big the time window is to shift the demand backwards"""
    DriveToPickUpTime = timedelta(minutes = abs(DropOffLoc - PickUpLoc))
    TotalTime = DriveToPickUpTime + DrivingTime
    d1_ts = time.mktime(PickUpFrom.timetuple())
    d2_ts = time.mktime((DropOfUntil - TotalTime).timetuple())
    PickUpWindow =  int(d2_ts-d1_ts) // 60 #in minutes
    return TotalTime, DriveToPickUpTime, PickUpWindow


def ComputeNewStartTime(carLocation, DropOffLoc, dem):
    """ This computes the shifted start times of the next demand, because the car location changes
        Timedelta behaves weird with negative values, so we have to check for signs.
    """
    OldDriveToNextPickupTime = timedelta(minutes=np.abs(carLocation - dem.PickUpLoc))
    NewDriveToNextPickupTime = timedelta(minutes=np.abs(DropOffLoc - dem.PickUpLoc))
    if OldDriveToNextPickupTime > NewDriveToNextPickupTime:
        TimeToPickUpDiff = OldDriveToNextPickupTime - NewDriveToNextPickupTime
        NewStartTime = dem.DemandStart + TimeToPickUpDiff
    else:
        TimeToPickUpDiff = NewDriveToNextPickupTime - OldDriveToNextPickupTime
        NewStartTime = dem.DemandStart - TimeToPickUpDiff
    return NewStartTime


def CheckCarDemands(car, DrivingTime, PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc):
    """ Gets all scheduled demands for the current car and then checks for free slots that fit the given times.
        We also shift the start of the next demand to accomodate the position change of the car."""

    #Get all scheduled demands, that might overlap with current demand
    demands = Demand.query.filter( 
        (Demand.AssignedCar == car.id) 
        ).order_by(Demand.DemandStart).all()
    
    if len(demands) == 0: #if no other demand exist, we can schedule it right away
        DemandStart = PickUpFrom - timedelta(minutes = abs(car.Location - PickUpLoc) )
        DemandEnd = PickUpFrom + DrivingTime
        return [car.id, DemandStart, DemandEnd, np.abs(car.Location - PickUpLoc)]
    
    TotalTime, DriveToPickUpTime, PickUpWindow = \
        CompPickUpTimes(DrivingTime, PickUpFrom, DropOfUntil, demands[-1].DropOffLoc, PickUpLoc)
    
    NewStartTime = ComputeNewStartTime(car.Location, DropOffLoc, demands[0])
    if PickUpFrom + DrivingTime < NewStartTime:
        demands[0].DemandStart = NewStartTime
        db.session.commit()
        
        DemandStart = PickUpFrom - DriveToPickUpTime
        DemandEnd = DemandStart + TotalTime
        return [car.id, DemandStart, DemandEnd, np.abs(car.Location - PickUpLoc)]

    for delta in range(0,PickUpWindow,5): # go through the timewindow to check for another free slot
        d_dt = timedelta(minutes = delta )
        if PickUpFrom + d_dt - DriveToPickUpTime > demands[-1].DemandEnd:
            DemandStart = PickUpFrom + d_dt - DriveToPickUpTime
            DemandEnd = DemandStart + TotalTime
            return [car.id, DemandStart, DemandEnd, np.abs(demands[-1].DropOffLoc - PickUpLoc)]
    
    for k, dem in enumerate(demands[:-1]):
        TotalTime, DriveToPickUpTime, PickUpWindow = \
            CompPickUpTimes(DrivingTime, PickUpFrom, DropOfUntil, dem.DropOffLoc, PickUpLoc)
        
        NewStartTime = ComputeNewStartTime(dem.DropOffLoc, DropOffLoc, demands[k+1])
        
        for delta in range(0,PickUpWindow,5):
            d_dt = timedelta(minutes = delta )
            if PickUpFrom + d_dt - DriveToPickUpTime > dem.DemandEnd \
                and PickUpFrom + d_dt + DrivingTime < NewStartTime:
                demands[k+1].DemandStart = NewStartTime
                db.session.commit()
                
                DemandStart = PickUpFrom + d_dt - DriveToPickUpTime
                DemandEnd = DemandStart + TotalTime
                return [car.id, DemandStart, DemandEnd, np.abs(dem.DropOffLoc - PickUpLoc)]
    return None

def ScheduleDemand(PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc, Seats, Engine, InfoSys):
    """ Gets all cars that fulfill demand and checks their schedule"""

    DrivingTime = CheckDrivingTime(PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc)
    
    #get cars hat fulfills requirement, sorted by distance to user
    carID = car_search(Seats, Engine, InfoSys, PickUpLoc)
    
    Cars = []
    #check for demands for each car.
    for car in carID:
        ret = CheckCarDemands(car, DrivingTime, PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc)
        if ret != None:
            Cars.append(ret)
    
    if len(Cars) == 0:
        return np.array([-1])
    
    #Get car with the minimal distance
    Cars = np.array(Cars)
    MinDist = np.argmin(Cars[:,-1])
    
    return Cars[MinDist]

def CheckDrivingTime(PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc):
    """ Checks if the time it takes to get from PickUp to DropOff location is larger than
        the times given with the demand"""
    DrivingTime = timedelta(minutes = abs(DropOffLoc - PickUpLoc)/1.)
    if DrivingTime > DropOfUntil - PickUpFrom:
        abort(406, 'Time to short to drive distance.')
    return DrivingTime

@app.route("/demand", methods=["POST"])
def demand_add():
    """ Adds new demand, if there is an available car, that fits all the requirements"""
    content = request.json
    email = JsonValRequired(content, 'email')
    CheckUser(email)
    
    PickUpFrom = JsonValRequired(content, 'PickUpFrom')
    PickUpFrom = datetime.strptime(PickUpFrom, '%Y-%m-%d %H:%M')
    DropOfUntil = JsonValRequired(content, 'DropOfUntil')
    DropOfUntil = datetime.strptime(DropOfUntil, '%Y-%m-%d %H:%M')
    CheckDemandTime(PickUpFrom, DropOfUntil)
    
    PickUpLoc = float(JsonValRequired(content, 'PickUpLoc'))
    DropOffLoc = float(JsonValRequired(content, 'DropOffLoc'))
    Seats = int(content.get('Seats', 1))
    Engine = int(content.get('Engine', 1))
    InfoSys = content.get('InfoSys', None)
    InfoSys = InfoSysToInt(InfoSys)
    
    DemandDet = ScheduleDemand(PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc, Seats, Engine, InfoSys)
    if DemandDet[0] == -1:
        abort(406, 'No cars are free for your request')
    
    new_demand = Demand(email, DemandDet[0], DemandDet[1], DemandDet[2], PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc, Seats, Engine, InfoSys)
    
    db.session.add(new_demand)
    db.session.commit()

    return demand_schema.jsonify(new_demand)

@app.route("/demand/DelAll", methods=["DELETE"])
def demand_delete_all():
    demands = Demand.query.all()
    for demand in demands:
        db.session.delete(demand)
    db.session.commit()

    return demands_schema.jsonify(demands)
    
@app.route("/demand/<id>", methods=["DELETE"])
def demand_delete(id):
    demand = Demand.query.get(id)
    CheckDemandNoQuery(demand, id)
    
    db.session.delete(demand)
    db.session.commit()

    return demand_schema.jsonify(demand)

@app.route("/demand/<id>", methods=["PUT"])
def demand_update(id):
    """ This one is a bit tricky. We read all the values given in the request and fill the missing values with the existing
        date from the to-be-updated demand. We then delete the old demand and check if there's a free slot. Then commit a new
        demand with either the updated values (if there is a free slot) or the old values (if not). Afterwards we change the id
        back to the id of the original demand.
    """
    demand = Demand.query.get(id)
    CheckDemandNoQuery(demand, id) 
    
    content = request.json
    #We need to check if the times still make sense (pickup < dropoff). 
    #Compare with old values if new ones aren't supplied 
    PickUpFrom = content.get('PickUpFrom', None)
    if PickUpFrom != None:
        PickUpFrom = datetime.strptime(PickUpFrom, '%Y-%m-%d %H:%M')
    else:
        PickUpFrom = demand.PickUpFrom
    
    DropOfUntil = content.get('DropOfUntil', None)
    if DropOfUntil != None:
        DropOfUntil = datetime.strptime(DropOfUntil, '%Y-%m-%d %H:%M')
    else:
        DropOfUntil = demand.DropOfUntil
    CheckDemandTime(PickUpFrom, DropOfUntil)
    
    PickUpLoc = content.get('PickUpLoc', None)
    if PickUpLoc == None:
        PickUpLoc = demand.PickUpLoc
    PickUpLoc = float(PickUpLoc) 
    DropOffLoc = content.get('DropOffLoc', None)
    if DropOffLoc == None:
        DropOffLoc = demand.DropOffLoc
    DropOffLoc = float(DropOffLoc) 
    
    Seats = content.get('Seats', None)
    if Seats == None:
        Seats = demand.Seats
    Seats = int(Seats) 
    Engine = content.get('Engine', None)
    if Engine == None:
        Engine = demand.Engine
    Engine = int(Engine) 
    InfoSys = content.get('InfoSys', None)
    InfoSys = InfoSysToInt(InfoSys)
    
    db.session.delete(demand)
    db.session.commit()
    DemandDet = ScheduleDemand(PickUpFrom, DropOfUntil, PickUpLoc, DropOffLoc, Seats, Engine, InfoSys)
    if DemandDet[0] == -1: 
        new_demand = Demand(demand.email, demand.AssignedCar, demand.DemandStart, demand.DemandEnd, demand.PickUpFrom, 
                        demand.DropOfUntil, demand.PickUpLoc, demand.DropOffLoc, demand.Seats, demand.Engine, demand.InfoSys)
    else:
        new_demand = Demand(demand.email, DemandDet[0], DemandDet[1], DemandDet[2], PickUpFrom, 
                        DropOfUntil, PickUpLoc, DropOffLoc, Seats, Engine, InfoSys)
    db.session.add(new_demand)
    db.session.commit()
    new_demand.id = demand.id
    db.session.commit()
    if DemandDet[0] == -1: 
        abort(406, 'Could not update session')
    return demand_schema.jsonify(new_demand)


##### USER FUNCTIONS ####
def CheckUser(email):
    """ Checks if a supplied email belongs to an user."""
    user = User.query.get(email)
    CheckUserNoQuery(user, email)

def CheckUserNoQuery(user, email):
    if user == None:
        abort(406, 'User {email} does not exists \n'.format(email=email))

def CheckAge(age):
    if age < 18:
        abort(406, 'User has to be older than 18\n')

@app.route("/user", methods=["POST"])
def user_add():
    """Adds new users. We require a unique email adress and check for underaged users"""
    content = request.json
    email = JsonValRequired(content, 'email')
    if User.query.get(email) != None:
        abort(406, 'User with email {email} already exists \n'.format(email=email))
    
    age = int(JsonValRequired(content, 'age'))
    CheckAge(age)
    
    name = JsonValRequired(content, 'name')
    gender = JsonValRequired(content, 'gender')
    new_user = User(email, name, gender, age)
    
    db.session.add(new_user)
    db.session.commit()

    return make_response('User with {email} successfully added'.format(email=email), 201)

@app.route("/user", methods=["GET"])
def users_get():
    all_users = User.query.all()
    result = users_schema.dump(all_users)
    return jsonify(result.data)

@app.route("/user/<email>", methods=["GET"])
def user_detail(email):
    user = User.query.get(email)
    CheckUserNoQuery(user, email)
    return user_schema.jsonify(user)

@app.route("/user/<email>", methods=["PUT"])
def user_update(email):
    """ Updates a users. Every entry can be changes.
        We require a unique email adress and check for underaged users."""
    user = User.query.get(email)
    CheckUserNoQuery(user, email) 
    content = request.json
    emailNew = content.get('email', None)
    name = content.get('name', None)
    gender = content.get('gender', None)
    age = content.get('age', None)
    
    if emailNew != None:
        if User.query.get(emailNew) != None:
            abort(406, 'User with new email {email} already exists \n'.format(email=emailNew))
        
        user.email = emailNew
    if name != None:
        user.name = name
    if gender != None:
        user.gender = gender
    if age != None:
        CheckAge(int(age))
        user.age = int(age)

    db.session.commit()
    return user_schema.jsonify(user)

@app.route("/user/<email>", methods=["DELETE"])
def user_delete(email):
    ActiveDemand = Demand.query.filter((Demand.email == email)).first()
    if ActiveDemand != None:
        abort(406, 'User {email} has an open demand\n'.format(email=email))
    
    user = User.query.get(email)
    CheckUserNoQuery(user, email)
    
    db.session.delete(user)
    db.session.commit()

    return user_schema.jsonify(user)

if __name__ == '__main__':
    app.run(debug=True)
